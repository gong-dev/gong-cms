<?php
namespace Gong\BaseCmsBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Yaml\Parser;
use Doctrine\ODM\MongoDB\Event\PreUpdateEventArgs;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;

class CmsEventListener
{
    protected $container;
    protected $templating;
    protected $myData;

    public function __construct(ContainerInterface $container = null, EngineInterface $templating = null)
    {
        $this->container  = $container;
        $this->templating = $templating;
    }

    /**
     * Add event listener for each controller use.
     */
    public function onKernelController($event)
    {
        $controller = $event->getController();
        $controller = current($controller);

        /* load media */
        $kernel = $this->container->get('kernel');
        $yaml = new Parser();
        $mediaConfigPath = $kernel->getRootDir().'/config/media.yml';

        $media = $yaml->parse(file_get_contents($mediaConfigPath));
        $this->container->set('media_config', $media);

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $routeName = $request->get('_route');

        if(isset($controller->crudConfig)){

            if($routeName == 'cms_cmsuser' || $routeName == 'cms_cmsuser_edit' || $routeName == 'cms_cmsuser_update'){
                $crudConfigPath = $kernel->locateResource('@BaseCmsBundle/Resources/config/crud/'.$controller->controllerName.'.yml');
            }else{
                $crudConfigPath = $kernel->locateResource('@CmsBundle/Resources/config/crud/'.$controller::ROUTE_NAME.'.yml');
            }

            $value = $yaml->parse(file_get_contents($crudConfigPath));

            $configArr = array();

            foreach($value['elements'] as $element){
                $configArr[$element['name']] = $element;
            }

            $value['elements'] = $configArr;

            $this->_crudConfig = $value;

            $controller->crudConfig = $value;
            $this->container->set('crud_config', $value);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $dm = $args->getDocumentManager();
        $document = $args->getDocument();
        $em = $dm->getEventManager();
        /* loop prevent */
        $em -> removeEventListener('postUpdate', $this);
        $em -> removeEventListener('postPersist', $this);

        $updated = false;

        if(method_exists($document, 'getSlug')){
            $document->setSlug($this->container->get('cocur_slugify')->slugify(strip_tags($document)));
            $updated = false;
        }

        if($updated){
            $dm->flush();
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $dm = $args->getDocumentManager();
        $document = $args->getDocument();
        $em = $dm->getEventManager();
        /* loop prevent */
        $em -> removeEventListener('postUpdate', $this);
        $em -> removeEventListener('postPersist', $this);

        $updated = false;

        if(method_exists($document, 'getSlug')){
            $document->setSlug($this->container->get('cocur_slugify')->slugify(strip_tags($document)));
            $updated = false;
        }

        if($updated){
            $dm->flush();
        }
    }
}