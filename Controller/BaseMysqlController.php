<?php

namespace Gong\BaseCmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseMysqlController extends Controller
{
    public $crudConfig = array();

    /**
     * Lists all enities.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()
            ->getRepository(static::ENTITY_BUNDLE_NAME.':'.static::ENTITY_NAME);

        $crud_filter = $this -> container ->get('cms.crud_filter')->init($repository->createQueryBuilder('p'), $em);

        $session = $request->getSession();

        $sortBy     = $request->query->get('sort', $this->crudConfig['default_sort_by']);
        $sortOrder  = $request->query->get('direction', $this->crudConfig['default_sort_dir']);

        $request->query->set('sort', $sortBy);
        $request->query->set('direction', $sortOrder);

        $filter = $request->query->get('filter');
        $search = $request->request->get('search');
        $clearSearch = $request->request->get('clear_search');

        /* wyszukiwanie */
        if(!is_null($clearSearch)){
            $session->remove('search_'.static::ROUTE_NAME);
            $search = NULL;
        }elseif(!is_null($search) && $search == ''){
            $session->remove('search_'.static::ROUTE_NAME);
            $search = NULL;
        }elseif (!is_null($search)) {
            $session->set('search_'.static::ROUTE_NAME, $search);
        }else{
            $search = $session->get('search_'.static::ROUTE_NAME);
        }

        if(is_null($filter)){
            $filter = (!is_null($session->get('filter_'.static::ROUTE_NAME)) ? $session->get('filter_'.static::ROUTE_NAME) : 'all');
        }else{
            $session->set('filter_'.static::ROUTE_NAME, $filter);
        }

        $q = $repository->createQueryBuilder('p')->orderBy($sortBy, $sortOrder);

        if(isset($this -> crudConfig['filters']))
            $q=  $crud_filter->prepareQuery($q, $this -> crudConfig['filters'], $filter);


        if(!is_null($search)){
            $searchn = 0;
            foreach ($this->crudConfig['elements'] as $value) {
                $q->orWhere('p.'.$value['name'].' LIKE :search'.$searchn)->setParameter('search'.$searchn, '%'.$search.'%');
                $searchn++;
            }
        }
        $res = $q->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $res,
            $request->query->get('page', 1),
            $this->crudConfig['elements_per_page']
        );

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate(static::ROUTE_NAME));

        $filters = array();
        if(isset($this -> crudConfig['filters']))
            $filters = $crud_filter->operateFilter($this -> crudConfig['filters'], $filter);

        $sortableElements = array();

        if(static::IS_SORTABLE)
            $sortableElements  = $repository->createQueryBuilder('p')->orderBy('p.sort', 'ASC')->getQuery()->getResult();

        return $this->render('BaseCmsBundle:Base:index.html.twig', array('is_sortable' => static::IS_SORTABLE, 'sortable_elements' => $sortableElements, 'pagination' => $pagination, 'filters' => $filters, 'search' => $search, 'crud_config' => $this->crudConfig, 'controller_name' => static::ROUTE_NAME));
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @param string $id The entity ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If entity doesn't exists
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(static::ENTITY_BUNDLE_NAME.':'.static::ENTITY_NAME)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $formName = static::FORM_TYPE_NAME;

        $editForm = $this->createForm(new $formName($this->crudConfig, $this->container->get('media_config')), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate(static::ROUTE_NAME));
        $breadcrumbs->addItem("Edytuj", $this->get("router")->generate(static::ROUTE_NAME."_edit", array('id' => $id)));

        return $this->render('BaseCmsBundle:Base:edit.html.twig', array(
            'document'    => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }

    /**
     * Deletes a entity.
     *
     * @param Request $request The request object
     * @param string $id       The entity ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If entity doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository(static::ENTITY_BUNDLE_NAME.':'.static::ENTITY_NAME)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $dashboard = $this->container->get('cms.dashboard');
                $dashboard->addData($this->getUser()->getId(), 'delete', static::ENTITY_NAME, $entity, static::ROUTE_NAME, $entity->getId());

                $em->remove($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    static::ITEM_DELETED_MSG
                );
            }
            return $this->redirect($this->get("router")->generate(static::ROUTE_NAME));
        }

        return $this->render('BaseCmsBundle:Base:delete_confirm.html.twig', array(
            'document' => $entity,
            'delete_form'     => $form->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    /**
     * Creates a new entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entityName = static::ENTITY_BUNDLE_NAME."\\".'Entity'."\\".static::ENTITY_NAME;
        $formName = static::FORM_TYPE_NAME;

        $entity = new $entityName();
        $form     = $this->createForm(new $formName($this->crudConfig, $this->container->get('media_config')), $entity);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid() ) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    static::ITEM_ADDED_MSG
                );

                $dashboard = $this->container->get('cms.dashboard');
                $dashboard->addData($this->getUser()->getId(), 'add', static::ENTITY_NAME, $entity, static::ROUTE_NAME, $entity->getId());

                return $this->redirect($this->get("router")->generate(static::ROUTE_NAME));
            }else{
                $this->get('session')->getFlashBag()->add(
                    'alert',
                    'Formularz zawiera błędy.'
                );
            }
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate(static::ROUTE_NAME));
        $breadcrumbs->addItem("Dodaj", $this->get("router")->generate(static::ROUTE_NAME."_create"));

        return $this->render('BaseCmsBundle:Base:new.html.twig', array(
            'document' => $entity,
            'form'     => $form->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    /**
     * Edits an existing entity.
     *
     * @param Request $request The request object
     * @param string $id       The entity ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If entity doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $formName = static::FORM_TYPE_NAME;

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(static::ENTITY_BUNDLE_NAME.':'.static::ENTITY_NAME)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new $formName($this->crudConfig, $this->container->get('media_config')), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                static::ITEM_UPDATED_MSG
            );

            $dashboard = $this->container->get('cms.dashboard');
            $dashboard->addData($this->getUser()->getId(), 'edit', 'Example', $entity, static::ROUTE_NAME,  $entity->getId());

            return $this->redirect($this->get("router")->generate(static::ROUTE_NAME.'_edit', array('id' => $id)));
        }else{
            $this->get('session')->getFlashBag()->add(
                'error',
                'Formularz zawiera błędy.'
            );
        }

        return $this->render('BaseCmsBundle:Base:edit.html.twig', array(
            'document'    => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    /**
     * Sorts items.
     *
     * @param Request $request The request object
     * @param array $items Items to sort
     *
     */
    public function sortAction(Request $request)
    {
        $response   = new JsonResponse();
        $result     = array('success' => false);

        if(static::IS_SORTABLE){
            $elements = $request->get('items', '');

            if($elements == '' || count($elements) == 0){
                $response->setData($result);
                return $response;
            }

            $em = $this->getDoctrine()->getManager();

            $repository = $this->getDoctrine()
                ->getRepository(static::ENTITY_BUNDLE_NAME.':'.static::ENTITY_NAME);

            $sortableSlider  = $repository->createQueryBuilder('p')->where('p.active = :par_active')->setParameter('par_active', true)->orderBy('p.sort', 'ASC')->getQuery()->getResult();

            foreach($sortableSlider as $entity){
                $index = array_search($entity->getId(), $elements);
                if($index !== false){
                    $entity->setSort($index);
                }
            }

            $em->flush();

            $result['success'] = true;
        }

        $response->setData($result);
        return $response;
    }
}