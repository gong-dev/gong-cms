<?php

namespace Gong\BaseCmsBundle\Controller;

use BaseCmsBundle\Document\CmsDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem("Ostatnia aktywność", $this->get("router")->generate("gong_cms_homepage"));

        $dm = $this->get('doctrine_mongodb')->getManager();
        $elementAttr = array();

        $modules = $this->_getIcons();

        /*
         * Search for single date history
         */
        $request = Request::createFromGlobals();
        $filter = false;
        $date = false;
        if($request->query->get('filterdate'))
        {
            $date = $request->query->get('filterdate');
            $filter = array('from' => new \DateTime($date), 'to'=>new \DateTime($date." 23:59:59"));
        }

        $query = $dm->createQueryBuilder('BaseCmsBundle:CmsDashboard');

        if($filter) {
            $query->field('created_at')->gte($filter['from']);
            $query->field('created_at')->lt($filter['to']);
        }
        else {
            $query->limit(50);
        }
        $query->sort('created_at', 'DESC');

        $dashboardData = $query->getQuery()->execute();

        return $this->render('BaseCmsBundle:Default:index.html.twig', array('dashboard_modules' => $modules, 'dashboard_data' => $dashboardData, 'search_date'=>$date, 'dashboard_config' => $elementAttr));
    }

    /**
     * Gets modules data.
     *     *
     * @return array $modules
     *
     */
    private function _getIcons(){
        $kernel = $this->get('kernel');

        $crudConfigPath = $kernel->locateResource('@BaseCmsBundle/Resources/config/crud');

        $finder = new Finder();
        $finder->files()->in($crudConfigPath)->name('*.yml');

        $modules = array();
        $yaml = new Parser();

        foreach ($finder as $file) {
            $value = $yaml->parse(file_get_contents($file->getRealpath()));

            $fileData = explode('/', $file->getRealpath());
            $name = str_replace('.yml', '', end($fileData));

            $modules[$name] = array('element_name' => $value['element_name'],
                'icon_class' => $value['icon_class'],
                'dashboard_description' => $value['dashboard_description']);
        }

        try{
            $childCrudConfigPath = $kernel->locateResource('@CmsBundle/Resources/config/crud');
            $finder->files()->in($childCrudConfigPath)->name('*.yml');

            foreach ($finder as $file) {
                $value = $yaml->parse(file_get_contents($file->getRealpath()));

                $fileData = explode('/', $file->getRealpath());
                $name = str_replace('.yml', '', end($fileData));

                $modules[$name] = array('element_name' => $value['element_name'],
                    'icon_class' => $value['icon_class'],
                    'dashboard_description' => $value['dashboard_description']);
            }

        }catch (\Exception $e){
            return $modules;
        }

        return $modules;
    }
}