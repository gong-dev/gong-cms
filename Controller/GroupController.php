<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Gong\BaseCmsBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterGroupResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseGroupEvent;
use FOS\UserBundle\Event\GroupEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormBuilderInterface as FormB;

/**
 * RESTful controller managing group CRUD
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class GroupController extends Controller
{
    /**
     * Show all groups
     */
    public function listAction()
    {
        if(!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $url = $this->generateUrl('gong_cms_homepage', array());
            $response = new RedirectResponse($url);
            return $response;
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem("Grupy użytkowników", $this->get("router")->generate("fos_user_group_list"));

        $groups = $this->get('fos_user.group_manager')->findGroups();


        $user = $this->getUser();

        // $users = $this->get('fos_user.user_manager')->findUserByEmail('michal.piontek@gong.pl')->;

        return $this->render('FOSUserBundle:Group:list.html.twig', array(
            'groups' => $groups
        ));
    }

    /**
     * Show one group
     */
    public function showAction($groupName)
    {
        if(!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $url = $this->generateUrl('gong_cms_homepage', array());
            $response = new RedirectResponse($url);
            return $response;
        }

        $group = $this->findGroupBy('name', $groupName);

        return $this->render('FOSUserBundle:Group:show.html.twig', array(
            'group' => $group
        ));
    }

    /**
     * Edit one group, show the edit form
     */
    public function editAction(Request $request, $groupName)
    {
        if(!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $url = $this->generateUrl('gong_cms_homepage', array());
            $response = new RedirectResponse($url);
            return $response;
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem("Grupy użytkowników", $this->get("router")->generate("fos_user_group_list"));


        $routingList = $this->container->get('cms.acl')->getCmsRouteList();

        $group = $this->findGroupBy('name', $groupName);

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseGroupEvent($group, $request);
        $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        //$formFactory = $this->get('fos_user.group.form.factory');

        $form = $this->createFormBuilder(array());

        //$form->add('name', 'text', array('required' => true, 'data' => $group->getName()));


        foreach($routingList as $route){

            if(in_array($route, $group->getRoles())){
                $form->add('privilage_'.$route, CheckboxType::class, array('label' => $route, 'required' => false, 'data' => true));
            }else{
                $form->add('privilage_'.$route, CheckboxType::class, array('label' => $route, 'required' => false, 'data' => false));
            }
        }

        $form->setAction($this->generateUrl('fos_user_group_edit', ['groupName'=>$group->getName()]));

        $form = $form->getForm();

        if ($this->container->get('request_stack')->getCurrentRequest()->getMethod() == 'POST') {
            $groupPrivilages = array();

            foreach($_POST['form'] as $key => $privilage){
                if(strpos($key, 'privilage_') !== FALSE)
                    array_push($groupPrivilages, str_replace('privilage_', '', $key));
            }

            $form->bind($request);
            /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
            $groupManager = $this->get('fos_user.group_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_SUCCESS, $event);

            $group->setRoles($groupPrivilages);
            $groupManager->updateGroup($group);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_group_list', array());
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

            return $response;
        }

        return $this->render('FOSUserBundle:Group:edit.html.twig', array(
            'form'      => $form->createview(),
            'group_name'  => $group->getName(),
        ));
    }

    /**
     * Show the new form
     */
    public function newAction(Request $request)
    {

        if(!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $url = $this->generateUrl('gong_cms_homepage', array());
            $response = new RedirectResponse($url);
            return $response;
        }
        /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
        $groupManager = $this->get('fos_user.group_manager');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.group.form.factory');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $group = $groupManager->createGroup('');

        $routingList = $this->container->get('cms.acl')->getCmsRouteList();

        $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_INITIALIZE, new GroupEvent($group, $request));

        $form = $this->createFormBuilder(array());

        $form->add('name', TextType::class, array('required' => true, 'data' => $group->getName()));
        $form->setAction($this->generateUrl('fos_user_group_new'));


        foreach($routingList as $route){
            $form->add('privilage_'.$route, CheckboxType::class, array('label' => $route, 'required' => false, 'data' => false));
        }

        $form = $form->getForm();

        if ($this->container->get('request_stack')->getCurrentRequest()->getMethod() == 'POST') {

            $groupPrivilages = array();

            foreach($_POST['form'] as $key => $privilage){
                if(strpos($key, 'privilage_') !== FALSE)
                    array_push($groupPrivilages, str_replace('privilage_', '', $key));
            }

            $group->setName($_POST['form']['name']);

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_SUCCESS, $event);

            $group->setRoles($groupPrivilages);
            $groupManager->updateGroup($group);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_group_list');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));
            return $response;
        }

        return $this->render('FOSUserBundle:Group:new.html.twig', array(
            'form' => $form->createview(),
        ));
    }

    /**
     * Delete one group
     */
    public function deleteAction(Request $request, $groupName)
    {
        if(!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $url = $this->generateUrl('gong_cms_homepage', array());
            $response = new RedirectResponse($url);
            return $response;
        }

        $group = $this->findGroupBy('name', $groupName);
        $this->get('fos_user.group_manager')->deleteGroup($group);

        $response = new RedirectResponse($this->generateUrl('fos_user_group_list'));

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(FOSUserEvents::GROUP_DELETE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

        return $response;
    }

    /**
     * Find a group by a specific property
     *
     * @param string $key   property name
     * @param mixed  $value property value
     *
     * @throws NotFoundHttpException                if user does not exist
     * @return \FOS\UserBundle\Model\GroupInterface
     */
    protected function findGroupBy($key, $value)
    {
        if (!empty($value)) {
            $group = $this->get('fos_user.group_manager')->{'findGroupBy'.ucfirst($key)}($value);
        }

        if (empty($group)) {
            throw new NotFoundHttpException(sprintf('The group with "%s" does not exist for value "%s"', $key, $value));
        }

        return $group;
    }
}
