<?php

namespace Gong\BaseCmsBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends Controller
{
    public $crudConfig = array();

    /**
     * Lists all documents.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();

        $crud_filter = $this -> container ->get('cms.crud_filter')->init($dm->createQueryBuilder(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME), $dm);

        $session = $request->getSession();

        $sortBy     = $request->query->get('sort', $this->crudConfig['default_sort_by']);
        $sortOrder  = $request->query->get('direction', $this->crudConfig['default_sort_dir']);

        $request->query->set('sort', $sortBy);
        $request->query->set('direction', $sortOrder);

        $filter = $request->query->get('filter');
        $search = $request->request->get('search');
        $clearSearch = $request->request->get('clear_search');

        /* wyszukiwanie */
        if(!is_null($clearSearch)){
            $session->remove('search_'.static::ROUTE_NAME);
            $search = NULL;
        }elseif(!is_null($search) && $search == ''){
            $session->remove('search_'.static::ROUTE_NAME);
            $search = NULL;
        }elseif (!is_null($search)) {
            $session->set('search_'.static::ROUTE_NAME, $search);
        }else{
            $search = $session->get('search_'.static::ROUTE_NAME);
        }

        if(is_null($filter)){
            $filter = (!is_null($session->get('filter_'.static::ROUTE_NAME)) ? $session->get('filter_'.static::ROUTE_NAME) : 'all');
        }else{
            $session->set('filter_'.static::ROUTE_NAME, $filter);
        }

        $q  = $dm->createQueryBuilder(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME);

        if(isset($this -> crudConfig['filters']))
            $q = $crud_filter->prepareQuery($q, $this -> crudConfig['filters'], $filter);

        $res = $q->sort($sortBy, $sortOrder);

        if(!is_null($search)){
            foreach ($this->crudConfig['elements'] as $value) {
                $res = $res->addOr($res->expr()->field($value['name'])->equals(new \MongoRegex('/.*'.$search.'.*/i')));
            }
        }

        $res = $res->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $res,
            $request->query->get('page', 1),
            $this->crudConfig['elements_per_page']
        );

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate(static::ROUTE_NAME));

        $filters = array();
        if(isset($this -> crudConfig['filters']))
            $filters = $crud_filter->operateFilter($this -> crudConfig['filters'], $filter);

        $sortableElements = array();

        if(static::IS_SORTABLE)
            $sortableElements  = $dm->createQueryBuilder(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME)->sort('sort', 'ASC')->getQuery()->execute();

        return $this->render('BaseCmsBundle:Base:index.html.twig', array('is_sortable' => static::IS_SORTABLE, 'sortable_elements' => $sortableElements, 'pagination' => $pagination, 'filters' => $filters, 'search' => $search, 'crud_config' => $this->crudConfig, 'controller_name' => static::ROUTE_NAME));
    }

    /**
     * Displays a form to edit an existing document.
     *
     * @param string $id The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();

        $document = $dm->getRepository(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME)->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        $formName = static::FORM_TYPE_NAME;
        // $formHelper = new $formName($this->crudConfig, $this->container->get('media_config'));

        $editForm = $this->createForm($formName, $document, [
            'crud_config' => $this->crudConfig,
            'media_config' => $this->container->get('media_config'),
            'action' => $this->generateUrl(sprintf('%s_update', static::ROUTE_NAME), ['id'=>$id]),
        ]);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate(static::ROUTE_NAME));
        $breadcrumbs->addItem("Edytuj", $this->get("router")->generate(static::ROUTE_NAME."_edit", array('id' => $id)));

        return $this->render('BaseCmsBundle:Base:edit.html.twig', array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->setAction($this->generateUrl(sprintf('%s_delete', static::ROUTE_NAME), ['id'=>$id]))
            ->add('id', HiddenType::class)
            ->getForm();
    }

    /**
     * Deletes a document.
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $document = $dm->getRepository(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME)->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $dashboard = $this->container->get('cms.dashboard');
                $dashboard->addData($this->getUser()->getId(), 'delete', static::DOCUMENT_NAME, $document, static::ROUTE_NAME, $document->getId());

                $dm->remove($document);
                $dm->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    static::ITEM_DELETED_MSG
                );
            }
            return $this->redirect($this->get("router")->generate(static::ROUTE_NAME));
        }

        return $this->render('BaseCmsBundle:Base:delete_confirm.html.twig', array(
            'document' => $document,
            'delete_form'     => $form->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    /**
     * Creates a new document.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $documentName = static::DOCUMENT_BUNDLE_NAME."\\".'Document'."\\".static::DOCUMENT_NAME;
        $formName = static::FORM_TYPE_NAME;

        $document = new $documentName();

        $form = $this->createForm($formName, $document, [
            'crud_config' => $this->crudConfig,
            'media_config' => $this->container->get('media_config'),
            'action' => $this->generateUrl(sprintf('%s_create', static::ROUTE_NAME)),
        ]);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid() ) {
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($document);
                $dm->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    static::ITEM_ADDED_MSG
                );

                $dashboard = $this->container->get('cms.dashboard');
                $dashboard->addData($this->getUser()->getId(), 'add', static::DOCUMENT_NAME, $document, static::ROUTE_NAME, $document->getId());

                return $this->redirect($this->get("router")->generate(static::ROUTE_NAME));
            }else{
                $this->get('session')->getFlashBag()->add(
                    'alert',
                    'Formularz zawiera błędy.'
                );
            }
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate(static::ROUTE_NAME));
        $breadcrumbs->addItem("Dodaj", $this->get("router")->generate(static::ROUTE_NAME."_create"));

        return $this->render('BaseCmsBundle:Base:new.html.twig', array(
            'document' => $document,
            'form'     => $form->createView(),
            'crud_config' => $this->crudConfig,
            'media_config' => $this->container->get('media_config'),
            'controller_name' => static::ROUTE_NAME
        ));
    }

    /**
     * Edits an existing document.
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $formName = static::FORM_TYPE_NAME;

        $dm = $this->get('doctrine_mongodb')->getManager();

        $document = $dm->getRepository(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME)->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm($formName, $document, [
            'crud_config' => $this->crudConfig,
            'media_config' => $this->container->get('media_config'),
            'action' => $this->generateUrl(sprintf('%s_update', static::ROUTE_NAME), ['id'=>$id]),
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                static::ITEM_UPDATED_MSG
            );

            $dashboard = $this->container->get('cms.dashboard');
            $dashboard->addData($this->getUser()->getId(), 'edit', 'Example', $document, static::ROUTE_NAME,  $document->getId());

            return $this->redirect($this->get("router")->generate(static::ROUTE_NAME.'_edit', array('id' => $id)));
        }else{
            $this->get('session')->getFlashBag()->add(
                'error',
                'Formularz zawiera błędy.'
            );
        }

        return $this->render('BaseCmsBundle:Base:edit.html.twig', array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => static::ROUTE_NAME
        ));
    }

    /**
     * Sorts items.
     *
     * @param Request $request The request object
     * @param array $items Items to sort
     *
     */
    public function sortAction(Request $request)
    {
        $response   = new JsonResponse();
        $result     = array('success' => false);

        if(static::IS_SORTABLE){
            $elements = $request->get('items', '');

            if($elements == '' || count($elements) == 0){
                $response->setData($result);
                return $response;
            }

            $dm = $this->get('doctrine_mongodb')->getManager();
            $sortableSlider  = $dm->createQueryBuilder(static::DOCUMENT_BUNDLE_NAME.':'.static::DOCUMENT_NAME)->field('active')->equals(true)->sort('sort', 'ASC')->getQuery()->execute();

            foreach($sortableSlider as $document){
                $index = array_search($document->getId(), $elements);
                if($index !== false){
                    $document->setSort($index);
                }
            }

            $dm->flush();

            $result['success'] = true;
        }

        $response->setData($result);
        return $response;
    }
}