<?php

namespace Gong\BaseCmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gong\BaseCmsBundle\Entity\CmsUser;
use Gong\BaseCmsBundle\Form\CmsMysqlUserType;

class CmsCmsMysqlUserController extends Controller
{
    public $controllerName = 'cms_cmsuser';
    public $crudConfig = array();

    /**
     * Lists all documents.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()
            ->getRepository('BaseCmsBundle:CmsUser');

        $crud_filter = $this -> container ->get('cms.crud_filter')->init($repository->createQueryBuilder('p'), $em);

        $session = $this->container->get('request_stack')->getCurrentRequest()->getSession();

        $sortBy     = $this->container->get('request_stack')->getCurrentRequest()->query->get('sort', $this->crudConfig['default_sort_by']);
        $sortOrder  = $this->container->get('request_stack')->getCurrentRequest()->query->get('direction', $this->crudConfig['default_sort_dir']);

        $this->container->get('request_stack')->getCurrentRequest()->query->set('sort', $sortBy);
        $this->container->get('request_stack')->getCurrentRequest()->query->set('direction', $sortOrder);

        $filter = $this->container->get('request_stack')->getCurrentRequest()->query->get('filter');
        $search = $this->container->get('request_stack')->getCurrentRequest()->request->get('search');
        $clearSearch = $this->container->get('request_stack')->getCurrentRequest()->request->get('clear_search');

        if(is_null($filter)){
            $filter = (!is_null($session->get('filter_'.$this->controllerName)) ? $session->get('filter_'.$this->controllerName) : 'all');
        }else{
            $session->set('filter_'.$this->controllerName, $filter);
        }

        $q = $repository->createQueryBuilder('p')->orderBy($sortBy, $sortOrder);

        if(isset($this -> crudConfig['filters']))
            $q=  $crud_filter->prepareQuery($q, $this -> crudConfig['filters'], $filter);


        if(!is_null($search)){
            $searchn = 0;
            foreach ($this->crudConfig['elements'] as $value) {
                $q->orWhere('p.'.$value['name'].' LIKE :search'.$searchn)->setParameter('search'.$searchn, '%'.$search.'%');
                $searchn++;
            }
        }
        $res = $q->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $res,
            $request->query->get('page', 1),
            $this->crudConfig['elements_per_page']
        );

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate($this->controllerName));

        $filters = array();
        if(isset($this -> crudConfig['filters']))
            $filters = $crud_filter->operateFilter($this -> crudConfig['filters'], $filter);

        $sortableElements = array();

        return $this->render('BaseCmsBundle:CmsUser:index.html.twig', array('sortable_elements' => $sortableElements, 'pagination' => $pagination, 'filters' => $filters, 'search' => $search, 'crud_config' => $this->crudConfig, 'controller_name' => $this->controllerName));
    }
    /**
    * Finds and displays a document.
    *
    * @param string $id The document ID
    *
    * @return \Symfony\Component\HttpFoundation\Response
    *
    * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
    */
    public function showAction($id)
    {
        $dm = $this->getDoctrine()->getManager();

        $document = $dm->getRepository('BaseCmsBundle:CmsUser')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate($this->controllerName));
        $breadcrumbs->addItem("Pokaż", $this->get("router")->generate($this->controllerName."_show", array('id' => $id)));

        return $this->render('BaseCmsBundle:CmsUser:show.html.twig', array(
                    'document' => $document,
                    'crud_config' => $this->crudConfig,
                    'controller_name' => $this->controllerName
                ));
    }

    /**
    * Displays a form to edit an existing document.
    *
    * @param string $id The document ID
    *
    * @return \Symfony\Component\HttpFoundation\Response
    *
    * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
    */
    public function editAction($id)
    {
        $dm = $this->getDoctrine()->getManager();

        $document = $dm->getRepository('BaseCmsBundle:CmsUser')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        $editForm = $this->createForm(new CmsMysqlUserType($this->crudConfig, $this->container->get('media_config')), $document);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate($this->controllerName));
        $breadcrumbs->addItem("Edytuj", $this->get("router")->generate($this->controllerName."_edit", array('id' => $id)));

        return $this->render('BaseCmsBundle:CmsUser:edit.html.twig', array(
                            'document'    => $document,
                            'edit_form'   => $editForm->createView(),
                            'delete_form' => $deleteForm->createView(),
                            'crud_config' => $this->crudConfig,
                            'controller_name' => $this->controllerName
                        ));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }

    /**
    * Deletes a document.
    *
    * @param Request $request The request object
    * @param string $id       The document ID
    *
    * @return \Symfony\Component\HttpFoundation\Response
    *
    * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
    */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);

        $dm = $this->getDoctrine()->getManager();
        $document = $dm->getRepository('BaseCmsBundle:CmsUser')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $dm->remove($document);
                $dm->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Element został usunięty.'
                );
            }
            return $this->redirect($this->get("router")->generate($this->controllerName));
        }

        return $this->render('BaseCmsBundle:CmsUser:delete_confirm.html.twig', array(
            'document' => $document,
            'delete_form'     => $form->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => $this->controllerName
        ));
    }

    /**
    * Creates a new document.
    *
    * @param Request $request
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function createAction(Request $request)
    {
        $document = new CmsUser();
        $form     = $this->createForm(new CmsMysqlUserType($this->crudConfig, $this->container->get('media_config')), $document);

        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid() ) {
                $dm = $this->getDoctrine()->getManager();
                $dm->persist($document);
                $dm->flush();

                $this->get('session')->getFlashBag()->add(
                'success',
                'Element został utworzony.'
                );

                return $this->redirect($this->get("router")->generate($this->controllerName));
            }
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("CMS", $this->get("router")->generate("gong_cms_homepage"));
        $breadcrumbs->addItem($this->crudConfig['list_title'], $this->get("router")->generate($this->controllerName));
        $breadcrumbs->addItem("Dodaj", $this->get("router")->generate($this->controllerName."_create"));

        return $this->render('BaseCmsBundle:CmsUser:new.html.twig', array(
            'document' => $document,
            'form'     => $form->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => $this->controllerName
        ));
    }

    /**
    * Edits an existing document.
    *
    * @param Request $request The request object
    * @param string $id       The document ID
    *
    * @return \Symfony\Component\HttpFoundation\Response
    *
    * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
    */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDoctrine()->getManager();

        $document = $dm->getRepository('BaseCmsBundle:CmsUser')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new CmsMysqlUserType($this->crudConfig, $this->container->get('media_config')), $document);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Element został zmodyfikowany.'
            );

            return $this->redirect($this->get("router")->generate($this->controllerName.'_edit', array('id' => $id)));
        }

        return $this->render('BaseCmsBundle:CmsUser:edit.html.twig', array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'crud_config' => $this->crudConfig,
            'controller_name' => $this->controllerName
        ));
    }
}