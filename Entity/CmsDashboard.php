<?php
/**
 * Created by PhpStorm.
 * User: michalpiontek
 * Date: 18.03.15
 * Time: 13:44
 */

namespace Gong\BaseCmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="cms_dashboard")
 */
class CmsDashboard
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $action = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $element_url = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $document_name = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $element_name = '';

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="Gong\BaseCmsBundle\Entity\CmsUser")
     * @ORM\JoinColumn(name="cms_user_id", referencedColumnName="id")
     **/
    public $cms_user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return CmsDashboard
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set element_url
     *
     * @param string $elementUrl
     * @return CmsDashboard
     */
    public function setElementUrl($elementUrl)
    {
        $this->element_url = $elementUrl;

        return $this;
    }

    /**
     * Get element_url
     *
     * @return string 
     */
    public function getElementUrl()
    {
        return $this->element_url;
    }

    /**
     * Set document_name
     *
     * @param string $documentName
     * @return CmsDashboard
     */
    public function setDocumentName($documentName)
    {
        $this->document_name = $documentName;

        return $this;
    }

    /**
     * Get document_name
     *
     * @return string 
     */
    public function getDocumentName()
    {
        return $this->document_name;
    }

    /**
     * Set element_name
     *
     * @param string $elementName
     * @return CmsDashboard
     */
    public function setElementName($elementName)
    {
        $this->element_name = $elementName;

        return $this;
    }

    /**
     * Get element_name
     *
     * @return string 
     */
    public function getElementName()
    {
        return $this->element_name;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return CmsDashboard
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set cms_user
     *
     * @param \Gong\BaseCmsBundle\Entity\CmsUser $cmsUser
     * @return CmsDashboard
     */
    public function setCmsUser(\Gong\BaseCmsBundle\Entity\CmsUser $cmsUser = null)
    {
        $this->cms_user = $cmsUser;

        return $this;
    }

    /**
     * Get cms_user
     *
     * @return \Gong\BaseCmsBundle\Entity\CmsUser
     */
    public function getCmsUser()
    {
        return $this->cms_user;
    }
}
