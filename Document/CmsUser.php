<?php
namespace Gong\BaseCmsBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class CmsUser extends BaseUser
{
    /** @MongoDB\Id(strategy="auto") */
    protected $id;


    /**
     * @MongoDB\ReferenceMany(targetDocument="Gong\BaseCmsBundle\Document\CmsGroup")
     */
    protected $groups;

    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Add group
     *
     * @param Gong\BaseCmsBundle\Document\CmsGroup $group
     */
    public function addGroup(\FOS\UserBundle\Model\GroupInterface $group)
    {
        $this->groups[] = $group;
    }

    /**
     * Remove group
     *
     * @param Gong\BaseCmsBundle\Document\CmsGroup $group
     */
    public function removeGroup(\FOS\UserBundle\Model\GroupInterface $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection $groups
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
