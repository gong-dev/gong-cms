<?php
namespace Gong\BaseCmsBundle\Document;

use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class CmsGroup extends BaseGroup
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Field(name="privilages", type="collection")
     */
    private $privilages;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set privilages
     *
     * @param collection $privilages
     * @return self
     */
    public function setPrivilages($privilages)
    {
        $this->privilages = $privilages;
        return $this;
    }

    /**
     * Get privilages
     *
     * @return collection $privilages
     */
    public function getPrivilages()
    {
        return $this->privilages;
    }

    public function __ToString(){
        return $this->getName();
    }
}
