<?php
/**
 * Created by PhpStorm.
 * User: michalpiontek
 * Date: 18.03.15
 * Time: 13:44
 */

namespace Gong\BaseCmsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Indexes;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Index;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceOne;


/**
 * @MongoDB\Document(repositoryClass="Gong\BaseCmsBundle\Repository\DashboardRepository")
 * @Indexes({
 *   @Index(keys={"created_at"="asc"})
 * })
 */
class CmsDashboard
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $action = '';

    /**
     * @MongoDB\Field(type="string")
     */
    protected $element_url = '';

    /**
     * @MongoDB\Field(type="string")
     */
    protected $element_id = '';

    /**
     * @MongoDB\Field(type="string")
     */
    protected $document_name = '';

    /**
     * @MongoDB\Field(type="string")
     */
    protected $element_name = '';

    /**
     * @MongoDB\Date
     */
    protected $created_at;

    /** @ReferenceOne(targetDocument="CmsUser", inversedBy="CmsUser") */
    public $cms_user;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * Get action
     *
     * @return string $action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set elementUrl
     *
     * @param string $elementUrl
     * @return self
     */
    public function setElementUrl($elementUrl)
    {
        $this->element_url = $elementUrl;
        return $this;
    }

    /**
     * Get elementUrl
     *
     * @return string $elementUrl
     */
    public function getElementUrl()
    {
        return $this->element_url;
    }

    /**
     * Set elementId
     *
     * @param string $elementId
     * @return self
     */
    public function setElementId($elementId)
    {
        $this->element_id = $elementId;
        return $this;
    }

    /**
     * Get elementId
     *
     * @return string $elementId
     */
    public function getElementId()
    {
        return $this->element_id;
    }

    /**
     * Set documentName
     *
     * @param string $documentName
     * @return self
     */
    public function setDocumentName($documentName)
    {
        $this->document_name = $documentName;
        return $this;
    }

    /**
     * Get documentName
     *
     * @return string $documentName
     */
    public function getDocumentName()
    {
        return $this->document_name;
    }

    /**
     * Set elementName
     *
     * @param string $elementName
     * @return self
     */
    public function setElementName($elementName)
    {
        $this->element_name = $elementName;
        return $this;
    }

    /**
     * Get elementName
     *
     * @return string $elementName
     */
    public function getElementName()
    {
        return $this->element_name;
    }

    /**
     * Set createdAt
     *
     * @param date $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set category
     *
     * @param Gong\BaseCmsBundle\Document\CmsUser $cms_user
     * @return self
     */
    public function setCmsUser(\Gong\BaseCmsBundle\Document\CmsUser $cms_user)
    {
        $this->cms_user = $cms_user;
        return $this;
    }

    /**
     * Get category
     *
     * @return Gong\BaseCmsBundle\Document\CmsUser $cms_user
     */
    public function getCmsUser()
    {
        return $this->cms_user;
    }
}
