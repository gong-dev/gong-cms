<?php
namespace Gong\BaseCmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CmsUserType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $myEntity = $builder->getForm()->getData();


        if($options['crud_config']['elements']['username']['editable'])
            $builder->add('username', null, array('label' => $options['crud_config']['elements']['username']['display_name']));

        if($options['crud_config']['elements']['usernameCanonical']['editable'])
            $builder->add('usernameCanonical', null, array('label' => $options['crud_config']['elements']['usernameCanonical']['display_name']));

        $builder->add('groups', null, array('required' => false, 'label' => 'Grupy'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gong\BaseCmsBundle\Document\CmsUser',
            'crud_config' => null,
            'media_config' => null,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gong\BaseCmsBundle\Document\CmsUser',
            'crud_config' => null,
            'media_config' => null
        ));
    }

    public function getName()
    {
        return 'gong_BaseCmsBundle_cmsusertype';
    }
}
