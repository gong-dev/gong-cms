<?php
namespace Gong\BaseCmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CmsMysqlUserType extends AbstractType
{
    private $_crudConfig;
    private $_mediaConfig;

    public function __construct($crudConfig = array(), $mediaConfig = array()){
        $this->_crudConfig = $crudConfig;
        $this->_mediaConfig = $mediaConfig;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $myEntity = $builder->getForm()->getData();

        
        if($this->_crudConfig['elements']['username']['editable'])
            $builder->add('username', null, array('label' => $this->_crudConfig['elements']['username']['display_name']));
        
        if($this->_crudConfig['elements']['usernameCanonical']['editable'])
            $builder->add('usernameCanonical', null, array('label' => $this->_crudConfig['elements']['usernameCanonical']['display_name']));

        $builder->add('groups', null, array('required' => false, 'label' => 'Grupy'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                            'data_class' => 'Gong\BaseCmsBundle\Entity\CmsUser'
        ));
    }

    public function getName()
    {
        return 'gong_BaseCmsBundle_cmsusertype';
    }
}
