<?php

namespace Gong\BaseCmsBundle\Service;

use Gong\BaseCmsBundle\Document\CmsDashboard as Dash;
use Symfony\Component\Yaml\Parser;

class CmsMedia
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    public $images;
    public $videos;
    public $files;

    private $kernel;

    public function __construct($kernel){
        $this->kernel = $kernel;
        $this->getMedia();

    }
    public function getMedia(){
        /* load media */
        $yaml = new Parser();
        $mediaConfigPath = $this->kernel->getRootDir().'/config/media.yml';

        $media = $yaml->parse(file_get_contents($mediaConfigPath));

        $this->images = $media['gong_cms']['images'];
        $this->videos = $media['gong_cms']['videos'];
        $this->files = $media['gong_cms']['files'];
    }

    public function getImageDir($file = '', $name = '', $size = 'normal'){

        if(isset($this->images[$name]['sizes'][$size]['dir']))
            return $this->images[$name]['sizes'][$size]['dir'].DIRECTORY_SEPARATOR.$file;

        return '';
    }
}