<?php

namespace Gong\BaseCmsBundle\Service;

use Gong\BaseCmsBundle\Entity\CmsDashboard as Dash;

class CmsMysqlDashboard
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    public function addData($idUser = '', $action = '', $entityName = '', $elementName = '', $elementUrl = '' )
    {
        $dm = $this->container->get('doctrine')->getManager();

        /* find user */
        $userData = $dm->getRepository('BaseCmsBundle:CmsUser')->findOneById($idUser);

        $dashboard = new Dash();
        $dashboard->setCreatedAt(new \DateTime());
        $dashboard->setAction($action);
        $dashboard->setDocumentName($entityName);
        $dashboard->setElementName($elementName);
        $dashboard->setElementUrl($elementUrl);
        $dashboard->setCmsUser($userData);
        $dm->persist($dashboard);

        $dm->flush();
    }
}