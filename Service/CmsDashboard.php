<?php

namespace Gong\BaseCmsBundle\Service;

use Gong\BaseCmsBundle\Document\CmsDashboard as Dash;

class CmsDashboard
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    public function addData($idUser = '', $action = '', $documentName = '', $elementName = '', $elementUrl = '' )
    {
        $dm = $this->container->get('doctrine_mongodb')->getManager();

        /* find user */
        $userData = $dm->getRepository('BaseCmsBundle:CmsUser')->findOneById($idUser);

        $dashboard = new Dash();
        $dashboard->setCreatedAt(new \DateTime());
        $dashboard->setAction($action);
        $dashboard->setDocumentName($documentName);
        $dashboard->setElementName($elementName);
        $dashboard->setElementUrl($elementUrl);
        $dashboard->setCmsUser($userData);
        $dm->persist($dashboard);

        $dm->flush();
    }
}