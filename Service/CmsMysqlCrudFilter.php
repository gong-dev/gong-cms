<?php

namespace Gong\BaseCmsBundle\Service;

use Doctrine\ORM\Tools\Pagination\Paginator;

class CmsMysqlCrudFilter
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    private $builder;
    private $dm;

    public function init($builder = NULL, $dm = NULL) {
        $this -> builder = $builder;
        $this -> dm      = $dm;
        return $this;
    }

    public function operateFilter($filters, $current_filter)
    {
        $ret = array();
        foreach ($filters as $filter) {
            if (isset($filter['dynamic'])) {
                if (method_exists($filter['class'], $filter['method'])) {
                    $new_filters = $this->dm->getRepository($filter['document'])->$filter['method']();
                    if (is_array($new_filters)) {
                        foreach ($new_filters as $new) {
                            $ret[$new['name']] = array(
                                'name' => $new['display_name'],
                                'count' => $this->countByFields($new['conditions']),
                                'active' => $current_filter == $new['name'],
                                'id' => $new['name']
                            );
                        }
                    }
                }
                continue;
            }

            $ret[$filter['name']] = array(
                'name' => $filter['display_name'],
                'count' => $this->countByFields($filter['conditions']),
                'active' => $current_filter == $filter['name'],
                'id' => $filter['name']
            );
        }
        return $ret;
    }

    public function prepareQuery($manager, $filters, $current_filter)
    {
        foreach ($filters as $filter) {
            if (isset($filter['dynamic'])) {
                if (method_exists($filter['class'], $filter['method'])) {
                    $new_filters = $this->dm->getRepository($filter['document'])->$filter['method']();
                    if (is_array($new_filters)) {
                        foreach ($new_filters as $new) {
                            if ($current_filter == $new['name']) {
                                return $this->queryByFields($manager, $new['conditions']);
                            }
                        }
                    }
                }
                continue;
            }

            if ($current_filter == $filter['name']) {
                return $this->queryByFields($manager, $filter['conditions']);
            }
        }
        return $manager;
    }

    private function queryByFields($manager, $conditions)
    {
        if (sizeOf($conditions)) {
            foreach ($conditions as $condition) {
                switch ($condition['expression']) {
                    case 'equal':
                    case '=':
                        $manager->where($condition['field'].' = :con')->setParameter('con', $condition['value']);
                        break;
                    case 'not-equal':
                    case '!=':
                        $manager->where($condition['field'].' != :con')->setParameter('con', $condition['value']);
                        break;
                    case 'gt':
                    case '>':
                        $manager->where($condition['field'].' > :con')->setParameter('con', $condition['value']);
                        break;
                    case 'lt':
                    case '<':
                        $manager->where($condition['field'].' < :con')->setParameter('con', $condition['value']);
                        break;
                    case 'gte':
                    case '>=':
                        $manager->where($condition['field'].' >= :con')->setParameter('con', $condition['value']);
                        break;
                    case 'lte':
                    case '<=':
                        $manager->where($condition['field'].' <= :con')->setParameter('con', $condition['value']);
                        break;
                }
            }
        }
        return $manager;
    }

    private function countByFields($conditions)
    {

        $q = clone $this->builder;

        if (sizeOf($conditions)) {
            foreach ($conditions as $condition) {
                switch ($condition['expression']) {
                    case 'equal':
                    case '=':
                        $q->where($condition['field'].' = :con')->setParameter('con', $condition['value']);
                        break;
                    case 'not-equal':
                    case '!=':
                        $q->where($condition['field'].' != :con')->setParameter('con', $condition['value']);
                        break;
                    case 'gt':
                    case '>':
                        $q->where($condition['field'].' > :con')->setParameter('con', $condition['value']);
                        break;
                    case 'lt':
                    case '<':
                        $q->where($condition['field'].' < :con')->setParameter('con', $condition['value']);
                        break;
                    case 'gte':
                    case '>=':
                        $q->where($condition['field'].' >= :con')->setParameter('con', $condition['value']);
                        break;
                    case 'lte':
                    case '<=':
                        $q->where($condition['field'].' <= :con')->setParameter('con', $condition['value']);
                        break;
                }
            }
        }

        $paginator = new Paginator($q, $fetchJoinCollection = true);

        return count($paginator);
    }
}