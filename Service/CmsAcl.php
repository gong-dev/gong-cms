<?php

namespace Gong\BaseCmsBundle\Service;

class CmsAcl
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    public function getCmsRouteList()
    {
        $collection = $this->container->get('router')->getRouteCollection();
        $allRoutes = $collection->all();

        $ret = array();

        foreach($allRoutes as $key => $value){
            if(substr($key, 0, 3) == 'cms')
                $ret[] = $key;
        }

        return $ret;
    }

    public function checkIfGroupHasAccess($route)
    {
        $collection = $this->container->get('router')->getRouteCollection();
        $allRoutes = $collection->all();

        foreach($allRoutes as $key => $value){
            if(substr($key, 0, 3) == 'cms')
                dump($key);
        }
    }
}