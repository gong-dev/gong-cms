<?php

namespace Gong\BaseCmsBundle\Service;

class CmsCrudFilter
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    private $builder;
    private $dm;

    public function init($builder = NULL, $dm = NULL) {
        $this -> builder = $builder;
        $this -> dm      = $dm;
        return $this;
    }

    public function operateFilter($filters, $current_filter)
    {
        $ret = array();
        foreach($filters as $filter)
        {
            if(isset($filter['dynamic']))
            {
                if(method_exists($filter['class'], $filter['method']))
                {
                    $new_filters = $this -> dm -> getRepository($filter['document'])->$filter['method']();
                    if(is_array($new_filters))
                    {
                        foreach ($new_filters as $new) {
                            $ret[$new['name']] = array(
                                'name'   => $new['display_name'],
                                'count'  => $this -> countByFields($new['conditions']),
                                'active' => $current_filter == $new['name'],
                                'id'     => $new['name']
                            );
                        }
                    }
                }
                continue;
            }

            $ret[$filter['name']] = array(
                'name'   => $filter['display_name'],
                'count'  => $this -> countByFields($filter['conditions']),
                'active' => $current_filter == $filter['name'],
                'id'     => $filter['name']
            );
        }
        return $ret;
    }

    public function prepareQuery($manager, $filters, $current_filter)
    {
        foreach($filters as $filter)
        {
            if(isset($filter['dynamic']))
            {
                if(method_exists($filter['class'], $filter['method']))
                {
                    $new_filters = $this -> dm -> getRepository($filter['document'])->$filter['method']();
                    if(is_array($new_filters))
                    {
                        foreach ($new_filters as $new) {
                            if($current_filter == $new['name'])
                            {
                                return $this -> queryByFields($manager, $new['conditions']);
                            }
                        }
                    }
                }
                continue;
            }

            if($current_filter == $filter['name'])
            {
                return $this -> queryByFields($manager, $filter['conditions']);
            }
        }
        return $manager;
    }

    private function queryByFields($manager, $conditions)
    {
        if(sizeOf($conditions))
        {
            foreach($conditions as $condition) {
                switch ($condition['expression']) {
                    case 'equal':
                    case '=':
                        $manager->field($condition['field'])->equals($condition['value']);
                        break;
                    case 'not-equal':
                    case '!=':
                        $manager->field($condition['field'])->notEqual($condition['value']);
                        break;
                    case 'gt':
                    case '>':
                        $manager->field($condition['field'])->gt($condition['value']);
                        break;
                    case 'lt':
                    case '<':
                        $manager->field($condition['field'])->lt($condition['value']);
                        break;
                    case 'gte':
                    case '>=':
                        $manager->field($condition['field'])->gte($condition['value']);
                        break;
                    case 'lte':
                    case '<=':
                        $manager->field($condition['field'])->lte($condition['value']);
                        break;
                }
            }
        }
        return $manager;
    }

    private function countByFields($conditions)
    {
        $q = clone $this -> builder;

        if(sizeOf($conditions))
        {
            foreach($conditions as $condition) {
                switch ($condition['expression']) {
                    case 'equal':
                    case '=':
                        $q->field($condition['field'])->equals($condition['value']);
                        break;
                    case 'not-equal':
                    case '!=':
                        $q->field($condition['field'])->notEqual($condition['value']);
                        break;
                    case 'gt':
                    case '>':
                        $q->field($condition['field'])->gt($condition['value']);
                        break;
                    case 'lt':
                    case '<':
                        $q->field($condition['field'])->lt($condition['value']);
                        break;
                    case 'gte':
                    case '>=':
                        $q->field($condition['field'])->gte($condition['value']);
                        break;
                    case 'lte':
                    case '<=':
                        $q->field($condition['field'])->lte($condition['value']);
                        break;
                }
            }
        }
        $q->count();

        return $q->getQuery()->execute();
    }
}