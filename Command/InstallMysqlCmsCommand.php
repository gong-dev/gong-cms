<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Gong\BaseCmsBundle\Command;

use Sensio\Bundle\GeneratorBundle\Manipulator\ConfigurationManipulator;
use Sensio\Bundle\GeneratorBundle\Model\Bundle;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\HttpKernel\KernelInterface;
use Sensio\Bundle\GeneratorBundle\Generator\BundleGenerator;
use Gong\BaseCmsBundle\Manipulator\KernelManipulator;
use Sensio\Bundle\GeneratorBundle\Manipulator\RoutingManipulator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Generates bundles.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class InstallMysqlCmsCommand extends GeneratorMysqlCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('gong:cms:install-mysql')
            ->setDescription('Install CMS - MySQL version')
            ->setDefinition(array(
                new InputOption('namespace', '', InputOption::VALUE_REQUIRED, 'The namespace of the bundle to create'),
                new InputOption('dir', '', InputOption::VALUE_REQUIRED, 'The directory where to create the bundle', 'src/'),
                new InputOption('bundle-name', '', InputOption::VALUE_REQUIRED, 'The optional bundle name'),
                new InputOption('format', '', InputOption::VALUE_REQUIRED, 'Use the format for configuration files (php, xml, yml, or annotation)'),
                new InputOption('shared', '', InputOption::VALUE_NONE, 'Are you planning on sharing this bundle across multiple applications?'),
            ))
            ->setHelp(<<<EOT
The <info>%command.name%</info> command helps you generates new bundles.

By default, the command interacts with the developer to tweak the generation.
Any passed option will be used as a default value for the interaction
(<comment>--namespace</comment> is the only one needed if you follow the
conventions):

<info>php %command.full_name% --namespace=Acme/BlogBundle</info>

Note that you can use <comment>/</comment> instead of <comment>\\ </comment>for the namespace delimiter to avoid any
problems.

If you want to disable any user interaction, use <comment>--no-interaction</comment> but don't forget to pass all needed options:

<info>php %command.full_name% --namespace=Acme/BlogBundle --dir=src [--bundle-name=...] --no-interaction</info>

Note that the bundle namespace must end with "Bundle".
EOT
            )
        ;
    }

    /**
     * @see Command
     *
     * @throws \InvalidArgumentException When namespace doesn't end with Bundle
     * @throws \RuntimeException         When bundle can't be executed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();

        $bundle = $this->createBundleObject($input);
        $questionHelper->writeSection($output, 'Bundle generation');

        /** @var BundleGenerator $generator */
        $generator = $this->getGenerator();

        $output->writeln(sprintf(
            '> Generating a sample bundle skeleton into <info>%s</info> <comment>OK!</comment>',
            $this->makePathRelative($bundle->getTargetDirectory())
        ));
        $generator->generateBundle($bundle);

        $errors = array();
        $runner = $questionHelper->getRunner($output, $errors);

        // check that the namespace is already autoloaded
        $runner($this->checkAutoloader($output, $bundle));

        // register the bundle in the Kernel class
        $runner($this->updateKernel($output, $this->getContainer()->get('kernel'), $bundle));

        // routing importing
        $runner($this->updateRouting($output, $bundle));

        if (!$bundle->shouldGenerateDependencyInjectionDirectory()) {
            // we need to import their services.yml manually!
            $runner($this->updateConfiguration($output, $bundle));
        }

        $questionHelper->writeGeneratorSummary($output, $errors);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();
        $questionHelper->writeSection($output, 'Welcome to the GONG CMS installer!');

        /*
         * shared option
         */
        $shared = $input->getOption('shared');
        $input->setOption('shared', 'no');

        $askForBundleName = true;

        // a simple application bundle
        $output->writeln(array(
            'Setting bundle name to <comment>CmsBundle</comment>.',
        ));

        $namespace = 'CmsBundle';

        $input->setOption('bundle-name', $namespace);
        $input->setOption('namespace', $namespace);
        $input->setOption('dir', 'src/');

        /*
         * format option
         */
        $format = $input->getOption('format');
        if (!$format) {
            $format = $shared ? 'xml' : 'annotation';
        }
        $output->writeln(array(
            '',
            'Setting your generated configuration to yml',
            '',
        ));

        $input->setOption('format', 'yml');
    }

    protected function checkAutoloader(OutputInterface $output, Bundle $bundle)
    {
        $output->write('> Checking that the bundle is autoloaded: ');
        if (!class_exists($bundle->getBundleClassName())) {
            return array(
                '- Edit the <comment>composer.json</comment> file and register the bundle',
                '  namespace in the "autoload" section:',
                '',
            );
        }
    }

    protected function updateKernel(OutputInterface $output, KernelInterface $kernel, Bundle $bundle)
    {
        $kernelManipulator = new KernelManipulator($kernel);

        $output->write(sprintf(
            '> Enabling the bundle inside <info>%s</info>: ',
            $this->makePathRelative($kernelManipulator->getFilename())
        ));

        try {
            $additionalBundles = array(
                $bundle->getBundleClassName(),
                'FOS\UserBundle\FOSUserBundle',
                'Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle',
                'Knp\Bundle\PaginatorBundle\KnpPaginatorBundle',
                'Knp\Bundle\MenuBundle\KnpMenuBundle',
                'WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle',
                'Liuggio\ExcelBundle\LiuggioExcelBundle',
                'FOS\JsRoutingBundle\FOSJsRoutingBundle',
                'JMS\TranslationBundle\JMSTranslationBundle',
                'Comur\ImageBundle\ComurImageBundle',
                'Stfalcon\Bundle\TinymceBundle\StfalconTinymceBundle',
                'Cocur\Slugify\Bridge\Symfony\CocurSlugifyBundle',
                'Symfony\Bundle\AsseticBundle\AsseticBundle'
            );

            $ret = $kernelManipulator->addBundles($additionalBundles);

            if (!$ret) {
                $reflected = new \ReflectionObject($kernel);

                return array(
                    sprintf('- Edit <comment>%s</comment>', $reflected->getFilename()),
                    '  and add the following bundle in the <comment>AppKernel::registerBundles()</comment> method:',
                    '',
                    sprintf('    <comment>new %s(),</comment>', $bundle->getBundleClassName()),
                    '',
                );
            }
        } catch (\RuntimeException $e) {
            return array(
                sprintf('Bundle <comment>%s</comment> is already defined in <comment>AppKernel::registerBundles()</comment>.', $bundle->getBundleClassName()),
                '',
            );
        }
    }

    protected function updateRouting(OutputInterface $output, Bundle $bundle)
    {
        $targetRoutingPath = $this->getContainer()->getParameter('kernel.root_dir').'/config/routing.yml';
        $output->write(sprintf(
            '> Importing the bundle\'s routes from the <info>%s</info> file: ',
            $this->makePathRelative($targetRoutingPath)
        ));
        $routing = new RoutingManipulator($targetRoutingPath);
        try {
            $ret = $routing->addResource($bundle->getName(), $bundle->getConfigurationFormat(), '/cms');

            $this->addMainRouting($output, $targetRoutingPath);
            $this->updateConfigurationFiles($output);
            $this->updateSecurityFile($output);
            $this->addMediaFile($output);

            if (!$ret) {
                if ('annotation' === $bundle->getConfigurationFormat()) {
                    $help = sprintf("        <comment>resource: \"@%s/Controller/\"</comment>\n        <comment>type:     annotation</comment>\n", $bundle->getName());
                } else {
                    $help = sprintf("        <comment>resource: \"@%s/Resources/config/routing.%s\"</comment>\n", $bundle->getName(), $bundle->getConfigurationFormat());
                }
                $help .= "        <comment>prefix:   /</comment>\n";

                return array(
                    '- Import the bundle\'s routing resource in the app\'s main routing file:',
                    '',
                    sprintf('    <comment>%s:</comment>', $bundle->getName()),
                    $help,
                    '',
                );
            }
        } catch (\RuntimeException $e) {
            return array(
                sprintf('Bundle <comment>%s</comment> is already imported.', $bundle->getName()),
                '',
            );
        }
    }

    protected function addMainRouting(OutputInterface $output, $path){
        $output->writeln('> Updating main routing.yml file.');

        $templateMain = __DIR__.'/../Resources/skeleton/mysql/bundle/routing_main.yml.twig';

        $mainRouting = file_get_contents($templateMain);

        file_put_contents($path, $mainRouting, FILE_APPEND);
    }

    protected function addMediaFile(OutputInterface $output){
        $output->writeln('> Creating media.yml file.');

        $targetConfigurationPath = $this->getContainer()->getParameter('kernel.root_dir').'/config/media.yml';

        $templateMedia = __DIR__.'/../Resources/skeleton/bundle/media.yml.twig';

        $mediaTemplate = file_get_contents($templateMedia);

        file_put_contents($targetConfigurationPath, $mediaTemplate);
    }

    protected function updateConfigurationFiles(OutputInterface $output)
    {
        $output->writeln('> Updating main config.yml file.');

        $targetConfigurationPath = $this->getContainer()->getParameter('kernel.root_dir').'/config/config.yml';

        $templateMainConfig = file_get_contents(__DIR__.'/../Resources/skeleton/mysql/bundle/config.yml.twig');

        file_put_contents($targetConfigurationPath, $templateMainConfig);

        //copy parameters
        $parametersFile = __DIR__.'/../Resources/skeleton/mysql/bundle/parameters.yml';
        $parametersDistFile = __DIR__.'/../Resources/skeleton/mysql/bundle/parameters.yml.dist';
        //copy fos templates
        $fosTemplateFile = __DIR__.'/../Resources/skeleton/FOSUserBundle';

        $fs = new Filesystem();

        $fs->copy($parametersFile, $this->getContainer()->getParameter('kernel.root_dir').'/config/parameters.yml');
        $fs->copy($parametersDistFile, $this->getContainer()->getParameter('kernel.root_dir').'/config/parameters.yml.dist');

        $output->writeln('> Initializing FOSUserBundle templates.');

        $fs->mirror($fosTemplateFile, $this->getContainer()->getParameter('kernel.root_dir').'/Resources/FOSUserBundle');
    }

    protected function updateSecurityFile(OutputInterface $output)
    {
        $output->writeln('> Updating main security.yml file.');

        $targetConfigurationPath = $this->getContainer()->getParameter('kernel.root_dir').'/config/security.yml';

        $templateSecurityConfig = file_get_contents(__DIR__.'/../Resources/skeleton/bundle/security.yml.twig');

        file_put_contents($targetConfigurationPath, $templateSecurityConfig);
    }

    protected function updateConfiguration(OutputInterface $output, Bundle $bundle)
    {
        $targetConfigurationPath = $this->getContainer()->getParameter('kernel.root_dir').'/config/config.yml';
        $output->write(sprintf(
            '> Importing the bundle\'s %s from the <info>%s</info> file: ',
            $bundle->getServicesConfigurationFilename(),
            $this->makePathRelative($targetConfigurationPath)
        ));
        $manipulator = new ConfigurationManipulator($targetConfigurationPath);
        try {
            $manipulator->addResource($bundle);
        } catch (\RuntimeException $e) {
            return array(
                '- Import the bundle\'s %s resource in the app\'s main configuration file:',
                '',
                $manipulator->getImportCode($bundle),
                '',
            );
        }
    }

    /**
     * Creates the Bundle object based on the user's (non-interactive) input.
     *
     * @param InputInterface $input
     *
     * @return Bundle
     */
    protected function createBundleObject(InputInterface $input)
    {
        $shared = $input->getOption('shared');
        $bundleName = $namespace = 'CmsBundle';

        $dir = $input->getOption('dir');
        if (null === $input->getOption('format')) {
            $input->setOption('format', 'annotation');
        }
        $format = Validators::validateFormat($input->getOption('format'));

        if (!$this->getContainer()->get('filesystem')->isAbsolutePath($dir)) {
            $dir = getcwd().'/'.$dir;
        }
        // add trailing / if necessary
        $dir = '/' === substr($dir, -1, 1) ? $dir : $dir.'/';

        return new Bundle(
            $namespace,
            $bundleName,
            $dir,
            $format,
            $shared
        );
    }

    protected function createGenerator()
    {
        return new BundleGenerator($this->getContainer()->get('filesystem'));
    }
}
