<?php

namespace Gong\BaseCmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Validator\Constraints\File;


class GenerateCrudCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('gong:cms:generate:mongo-crud')
            ->setDescription('Generate CRUD - MongoDB')
            ->addArgument(
                'document_name',
                InputArgument::REQUIRED,
                'Podaj nazwę dokumentu:');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $kernel             = $this->getContainer()->get('kernel');
        $documentName       = $input->getArgument('document_name');
        $annotationReader   = new AnnotationReader();
        $reflectionClass    = new \ReflectionClass($documentName);
        $isSortable         = false;

        $documentClassName = explode('\\',$documentName);
        $bundleName = '';

        $documentNameLen = count($documentClassName);
        $i = 0;
        foreach($documentClassName as $key => $value){
            $i++;

            if($value != 'Bundle' && $value != 'Document' && $i != $documentNameLen)
                $bundleName .= $value;
        }
        $documentClassName = end($documentClassName);

        $documentProperties = array();
        $allowedPropertyTypes  = array('string', 'int', 'date', 'boolean', 'float', 'one', 'integer');

        $properties = $reflectionClass->getProperties();

        try{
            $reflectionClass->getMethod('__ToString');
        }catch(\Exception $e){
            $output->writeln('<error>Dokument '.$documentClassName.' musi posiadać metodę __ToString()</error>');
            die();
        }

        foreach($properties as $property){
            $propertyAnnotations = null;
            $reflectionProperty = new \ReflectionProperty($documentName, $property->getName());
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);

            $propertyAnnotations = current($propertyAnnotations);

            if(isset($propertyAnnotations->type) && in_array($propertyAnnotations->type, $allowedPropertyTypes)){

                $docProp = array('name' => $property->getName(), 'type' => $propertyAnnotations->type, 'additional' => array());


                if($property->getName() == 'sort'){
                    $isSortable = true;
                    $output->writeln('<info>Wykryto pole <comment>sort</info>. Dodaję obsługę zmiany kolejności.');
                }

                if($propertyAnnotations->type == 'one' && isset($propertyAnnotations->inversedBy)) {
                    $docProp['additional'] = array('inversed_by' => $propertyAnnotations->inversedBy, 'target_document' => $propertyAnnotations->targetDocument);

                    $targetDocumentReflectionClass = new \ReflectionClass(str_replace($documentClassName, $propertyAnnotations->targetDocument, $documentName));

                    try{
                        $targetDocumentReflectionClass->getMethod('__ToString');
                    }catch(\Exception $e){
                        $output->writeln('<error>Dokument '.$propertyAnnotations->targetDocument.' musi posiadać metodę __ToString()</error>');
                        die();
                    }
                }
                $documentProperties[] = $docProp;
            }else{
                if($propertyAnnotations instanceof File){
                    $docProp = array('name' => $property->getName(), 'type' => 'file', 'additional' => array());
                    $documentProperties[] = $docProp;
                }
            }
        }

        $fs = new Filesystem();


        /* make file */
        try {
            $controllerPath= $kernel->locateResource('@CmsBundle').'/Controller/Cms'.$documentClassName.'Controller.php';
            $fs->touch($controllerPath);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
            die();
        }

        $output->writeln('<info>Generowanie kontrolera <comment>Cms'.$documentClassName.'Controller.php</comment>.</info>');

        /* get crud template */
        $crudTemplatesPath = $kernel->locateResource('@BaseCmsBundle/Resources/views/CrudGenerator/');

        $controllerText = $this->getContainer()->get('templating')->render('BaseCmsBundle:CrudGenerator:controller.php.twig',
            array('document_name' => $documentName,
                'default_sort_by' => $documentProperties[0]['name'],
                'document_class_name' => $documentClassName,
                'bundle_name' => $bundleName,
                'is_sortable' => $isSortable)
        );

        file_put_contents($controllerPath, $controllerText);

        /* make FormType.php.twig file */

        try {
            $formPath= $kernel->locateResource('@CmsBundle').'/Form';
            $fs->mkdir($formPath);
            $fs->touch($formPath.'/'.$documentClassName.'Type.php');
            //$formContent = file_get_contents($crudTemplatesPath.'FormType.php.twig');
            $formContent = $this->getContainer()->get('templating')->render('BaseCmsBundle:CrudGenerator:FormType.php.twig',
                array('document_name' => $documentName,
                    'default_sort_by' => $documentProperties[0]['name'],
                    'document_class_name' => $documentClassName,
                    'bundle_name' => $bundleName,
                    'document_properties' => $documentProperties)
            );
            file_put_contents($formPath.'/'.$documentClassName.'Type.php', $formContent);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
            die();
        }

        $output->writeln('<info>Generowanie szablonu formularza <comment>Form'.$documentClassName.'.php</comment>.</info>');

        /* make crud config file */
        $crudConfigPath = $kernel->locateResource('@CmsBundle/Resources/config');
        $fs->mkdir($crudConfigPath.'/crud');
        $fs->touch($crudConfigPath.'/crud/cms_'.strtolower($documentClassName).'.yml');

        $configText = $this->getContainer()->get('templating')->render('BaseCmsBundle:CrudGenerator:crud_settings.yml.twig',
            array('document_name' => $documentName,
                'default_sort_by' => $documentProperties[0]['name'],
                'document_class_name' => $documentClassName,
                'bundle_name' => $bundleName,
                'document_properties' => $documentProperties)
        );

        $output->writeln('<info>Generowanie pliku konfiguracji <comment>cms_'.strtolower($documentClassName).'.yml</comment>.</info>');

        file_put_contents($crudConfigPath.'/crud/cms_'.strtolower($documentClassName).'.yml', $configText);

        /* make crud routing file */
        $routingConfigPath = $kernel->locateResource('@CmsBundle/Resources/config');
        $fs->mkdir($routingConfigPath.'/routing');
        $fs->touch($routingConfigPath.'/routing/cms_'.strtolower($documentClassName).'.yml');

        $routingText = $this->getContainer()->get('templating')->render('BaseCmsBundle:CrudGenerator:routing.yml.twig',
            array('document_name' => $documentName,
                'default_sort_by' => $documentProperties[0]['name'],
                'document_class_name' => $documentClassName,
                'bundle_name' => $bundleName,
                'document_properties' => $documentProperties)
        );

        $output->writeln('<info>Generowanie pliku routingu <comment>cms_'.strtolower($documentClassName).'.yml</comment>.</info>');
        file_put_contents($routingConfigPath.'/routing/cms_'.strtolower($documentClassName).'.yml', $routingText);

        /* update main routing file */
        $mainRoutingText = $this->getContainer()->get('templating')->render('BaseCmsBundle:CrudGenerator:main_routing.yml.twig',
            array('document_name' => $documentName,
                'default_sort_by' => $documentProperties[0]['name'],
                'document_class_name' => $documentClassName,
                'bundle_name' => $bundleName,
                'document_properties' => $documentProperties)
        );

        $output->writeln('<info>Dodawanie wpisu w głównym pliku routingu <comment>routing.yml</comment>.</info>');
        file_put_contents($routingConfigPath.'/routing.yml', $mainRoutingText, FILE_APPEND);

        $output->writeln('<info>Lista CRUD dla dokumentu <comment>'.$documentClassName.'</comment> została utworzona.</info>');
    }
}