<?php

namespace Gong\BaseCmsBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class TwigCustomExtension extends \Twig_Extension
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('human_date', array($this, 'humanDate')),
            new \Twig_SimpleFilter('delete', array($this, 'delete')),
            new \Twig_SimpleFilter('clear', array($this, 'clear')),
            new \Twig_SimpleFilter('count', array($this, 'count')),
            new \Twig_SimpleFilter('decorate', array($this, 'decorate'))
        );
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('show_action_test', array($this, 'showActionCondition'))
       );
    }

    public function humanDate($date, $relative = true, $time = false, $day = false)
    {
        $months = array('', 'stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca', 'lipca', 'sierpnia', 'września', 'października', 'listopada', 'grudnia');
        $days = array('', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela');

        //2010-04-21 10:19:41
        $timestamp = strtotime($date);

        if ($timestamp == false) {
            return $date;
        }

        if ($relative == false || $relative === 'false') {
            return ($day ? ($days[date('N', $timestamp)] . ', ') : '') . date('j', $timestamp) . ' ' . $months [date('n', $timestamp)] . ' ' . date('Y', $timestamp) . ($time === true ? ', ' . date('H:i', $timestamp) : '');
        }

        switch (date('dmY', $timestamp)) {
            case date('dmY', time()):
                $ret = 'Dzisiaj';
                break;
            case date('dmY', strtotime('yesterday')):
                $ret = 'Wczoraj';
                break;
            case date('dmY', strtotime('-2 day')):
                $ret = 'Przedwczoraj';
                break;
            default:
                $ret = ($day ? $days[date('N', $timestamp)] . ', ' : '') . date('d', $timestamp) . ' ' . $months [date('n', $timestamp)] . ' ' . date('Y', $timestamp);
                break;
        }
        return $ret . ($time === true ? ', ' . date('H:i', $timestamp) : '');
    }

    public function delete($array, $key)
    {
        unset($array[$key]);
        return $array;
    }

    public function clear($text)
    {
        if (mb_strtoupper($text, 'utf-8') == $text)
            $text = $this->mbUcFirst($text, 'utf-8');

        $text = trim($text);

        $text = preg_replace('/  +/', ' ', $text);
        $text = preg_replace('/,([a-zA-Z])/', ', $1', $text);
        $text = preg_replace('/ ([,.])/', '$1', $text);

        return $text;
    }

    public function count($text)
    {
        $text = $this->clear($text);
        return mb_strlen($text, 'utf-8');
    }

    public function decorate($value, $document, $field)
    {
        $document_class = get_class($document);
        $decorator_class = strtr($document_class, array('\Document' => '\Decorator'));
        $decorator_class_mysql = strtr($document_class, array('\Entity' => '\Decorator'));

        $field = preg_replace_callback('/\.(.)/',
            function($element)
            {return strtoupper($element);},
            $field);
        $decorator_method = sprintf('%sDecorate', $field);

        if (method_exists($decorator_class, $decorator_method)) {
            return $decorator_class::$decorator_method($value, $document, $this->container);
        }elseif(method_exists($decorator_class_mysql, $decorator_method)){
            return $decorator_class_mysql::$decorator_method($value, $document, $this->container);
        }
        return $value;
    }

    /**
     * returns second parameter in type requested in the first one
     * @param $type bool, int or float requested type
     * @param $val value for transformation
     * @return mixed second parameter translated into requeste type
     */
    private function _toType($type, $val){
        $types = array(
            'bool' => boolval($val),
            'int' => intval($val),
            'float' => floatval($val)
        );

        if(array_key_exists($type, $types))
            return $types[$type];
        else
            return $val;
    }

    /**
     * compare two values with requested condition in requested type
     * @param $exp condition for comparing
     * @param $var1 value one for compare
     * @param $var2 value one for compare
     * @param $type bool, int or float requested type
     * @return bool compare result
     */
    private function _compareOne($exp, $var1, $var2, $type)
    {
        $var1 = $this->_toType($type, $var1);
        $var2 = $this->_toType($type, $var2);

        $conditions = array(
            '==' => $var1 == $var2,
            '>=' => $var2 >= $var2,
            '<=' => $var1 <= $var2,
            '!=' => $var1 != $var2,
            '>' => $var1 > $var2,
            '<' => $var1 < $var2
        );

        if(array_key_exists($exp, $conditions)) {
            return ($conditions[$exp]);
        }else
        {
            trigger_error("Expression not recognized");
            return false;
        }
    }

    public function showActionCondition($rulesTable, $element)
    {
        //field : 'rejected', expression: '==', value : false, type: bool, glue: 'and'

        $statements = array();
        $glue = 'and';
        if(!empty($rulesTable['conditions'])){
            foreach($rulesTable['conditions'] as $rule){
                //ORM methods embedded or flat
                if(strpos($rule['field'], '.'))
                {
                    $exploded = explode('.', $rule['field']);
                    $prop = "get" . ucfirst($exploded[0]);
                    $temp_field = $element->$prop();

                    for($j=1;$j<count($exploded); $j++) {
                        if($temp_field) {
                            $prop = "get" . ucfirst($exploded[$j]);
                            $temp_field = $temp_field->$prop();
                        }
                    }
                }
                else {
                    $prop = "get" . ucfirst($rule['field']);
                    $field = ($element->$prop()) ? $element->$prop() : 0;
                }

                $statements[] = $this->_compareOne($rule['expression'], $field, $rule['value'], $rule['type']);
                if(array_key_exists('glue', $rule))
                    $glue = $rule['glue'];
            }
        }
        else{
            //no conditions - show always
            return true;
        }

        //and or or
        if($glue=='and') {
            return (count(array_unique($statements)) === 1 && array_unique($statements)[0] == true );
        }
        else{
            return (in_array(true, $statements));
        }
    }


    public function getName()
    {
        return 'twig_custom_extension';
    }

    private function mbUcFirst($string, $encoding)
    {
        $string = mb_strtolower($string, $encoding);
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
}