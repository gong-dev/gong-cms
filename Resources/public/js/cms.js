$(function () {
    $('#dashoboardatepicker').datepicker({
        'locale': 'pl'
    });

    dashboardAjax();
});

function dashboardAjax()
{
    if($('.btn-group').length)
    {
        $('.btn-group .ajax-call').live('click', function(e){
            e.preventDefault();
            if($(this).hasClass('ajax-approve')) {
                var r = confirm("Potwierdź działanie");
                var txt = '';
                if (r == true) {
                    var parn = $(this);
                    $.ajax({
                        url: $(this).attr('href'),
                        context: document.body
                    }).done(function (data) {
                        if (data.success) {
                            parn.closest('tr').remove();
                        }
                        else {
                            if(data.info)
                                alert(data.info);
                        }
                    });
                } else {
                    txt = "You pressed Cancel!";
                }
            }else{
                var parn = $(this);
                $.ajax({
                    url: $(this).attr('href'),
                    context: document.body
                }).done(function (data) {
                    if (data.success) {
                        parn.closest('tr').remove();
                    }
                    else {
                        if(data.info)
                            alert(data.info);
                    }
                });
            }

        })
    }
}