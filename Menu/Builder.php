<?php

namespace Gong\BaseCmsBundle\Menu;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder
{
    use ContainerAwareTrait;

    private $factory;
    protected $requestStack;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, RequestStack $requestStack)
    {
        $this->factory = $factory;
        $this->requestStack = $requestStack;
    }

    public function mainMenu(array $options)
    {
        $menu = $this->factory->createItem('root', array('childrenAttributes' => array('class' => 'nav')));

        $menu->addChild('Dashboard', array('route' => 'gong_cms_homepage', 'linkAttributes' => array('iconClass' => 'glyphicon glyphicon-home')));
        $modules = $this->_getModules();

        $allowedRoutes = $this->container->get('security.token_storage')->getToken()->getUser()->getRoles();

        foreach($modules as $key => $module){
            if(is_null($module['menu_parent'])) {
                if($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN') or in_array($key, $allowedRoutes)) {
                    $menu->addChild($module['element_name'], array(
                        'route' => $key,
                        'attributes' => array('class' => 'has-sub'),
                        'childrenAttributes' => array('class' => 'sub-menu'),
                        'linkAttributes' => array('iconClass' => $module['icon_class'])
                    ));
                }
            }
        }

        foreach($modules as $key => $module) {
            if(!is_null($module['menu_parent'])){
                if($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN') or in_array($key, $allowedRoutes)) {
                    if (!isset($menu[$module['menu_parent']])) {
                        $menu->addChild($module['menu_parent'], array(
                            'route' => null,
                            'attributes' => array('class' => 'has-sub'),
                            'childrenAttributes' => array('class' => 'sub-menu'),
                            'linkAttributes' => array('iconClass' => 'fa fa-th')
                        ));
                        $menu[$module['menu_parent']]->addChild($module['element_name'],
                            array('route' => $key, 'attributes' => array('class' => 'has-sub')));
                    } else {
                        $menu[$module['menu_parent']]->addChild($module['element_name'],
                            array('route' => $key, 'attributes' => array('class' => 'has-sub')));
                    }
                }
            }
        }

        if($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $menu->addChild('Grupy użytkowników', array('route' => 'fos_user_group_list', 'linkAttributes' => array('iconClass' => 'glyphicon glyphicon-user')));
            $menu->addChild('Użytkownicy', array('route' => 'cms_cmsuser', 'linkAttributes' => array('iconClass' => 'glyphicon glyphicon-user')));
        }

        $request = $this->requestStack->getCurrentRequest();
        $routeName = $request->get('_route');

        $partUri = explode('/',str_replace(array('http://', 'https://'), array('', ''), $request->getUri()));

        foreach ($menu as $child) {
            $partUrl = explode('/',$child->getUri());

            if(count($partUrl) > 2 && count($partUri) > 2 && $partUri[2] == $partUrl[2]){
                $child->setCurrent(true);
            }
        }

        return $menu;
    }

    /**
     * Gets modules data.
     *     *
     * @return array $modules
     *
     */
    private function _getModules(){
        $kernel = $this->container->get('kernel');

        $modules = array();

        try{
            $crudConfigPath = $kernel->locateResource('@CmsBundle/Resources/config/crud');

            $finder = new Finder();
            $finder->files()->in($crudConfigPath)->name('*.yml');


            $yaml = new Parser();

            foreach ($finder as $file) {

                $value = $yaml->parse(file_get_contents($file->getRealpath()));

                $fileData = explode('/', $file->getRealpath());
                $name = str_replace('.yml', '', end($fileData));

                $modules[$name] = array('element_name' => $value['menu_name'],
                    'icon_class' => $value['icon_class'],
                    'dashboard_description' => $value['dashboard_description'],
                    'menu_parent' => (isset($value['menu_parent'])) ? $value['menu_parent'] : null
                );
            }

            return $modules;

        }catch(\Exception $e){
            return $modules;
        }
    }
}