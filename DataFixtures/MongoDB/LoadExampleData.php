<?php

namespace Gong\BaseCmsBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Document\Example;
use AppBundle\Document\ExampleCategory;
use BaseCmsBundle\Document\CmsUser;

class LoadExampleData implements FixtureInterface
{
    /**
    * {@inheritDoc}
    */
    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadExampleCategory($manager);
        $this->loadExample($manager);
    }

    private function loadExampleCategory(ObjectManager $manager){

        for($i = 0; $i < 10; $i++){
            $example = new ExampleCategory();

            $example->setName('Testowa kategoria '.$i);
            $example->setSlug('testowa-kategoria-'.$i);

            $manager->persist($example);
        }

        $manager->flush();
    }

    private function loadExample(ObjectManager $manager){

        for($i = 0; $i < 60; $i++){
            $example = new Example();

            $example->setCreatedAt(new \DateTime());

            if($i%2 == 0)
                $example->setActive(true);

            $example->setContent($i. ' Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
            $example->setName('Testowy element '.$i);
            $example->setSlug('testowy-element-'.$i);

            $category = $manager->getRepository('AppBundle:ExampleCategory')->findOneBy(array());

            $example->setCategory($category);

            $manager->persist($example);
        }

        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager){
        $user = new CmsUser();
        $user->setCredentialsExpired(false);
        $user->setEmail('dev@gong.pl');
        $user->setEmailCanonical('dev@gong.pl');
        $user->setEnabled(true);
        $user->setExpired(false);
        $user->setLastLogin(new \DateTime());
        $user->setLocked(false);
        $user->setPassword('4yrPKbcgNf2pHIUvt1r5x8KUiGE7gtkCw+l0siY0Z+eNSi0wZuEZm9fGW0BEJ51qyv5Xl2TINcYIeq/36Brtkw==');
        $user->setRoles(array());
        $user->setSalt('ho0tf1ir1q80w0g4s4kcsw4w848kc0o');
        $user->setUsername('gong');
        $user->setUsernameCanonical('gong');

        $manager->persist($user);
        $manager->flush();
    }
}